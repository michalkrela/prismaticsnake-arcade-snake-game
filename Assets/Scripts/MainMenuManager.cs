using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public Canvas WelcomeScreenCanvas;
    public Canvas MainMenuCanvas;

    public GameObject BtnPanel;

    public Animator _mainMenuAnim;
    public Animator _creditsAnim;
    public Animator _howToPlayAnim;

    private void Start()
    {
    }

    private void Update()
    {
        if (WelcomeScreenCanvas.enabled == true && Input.anyKey)
        {
            WelcomeScreenCanvas.enabled = false;
            MainMenuCanvas.enabled = true;

            _mainMenuAnim.SetTrigger("ShowMainMenu");
        }

    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowCredits()
    {
        _mainMenuAnim.ResetTrigger("ShowMainMenu");
        _mainMenuAnim.SetTrigger("HideMainMenu");
        _creditsAnim.SetTrigger("ShowCredits");
    }

    public void HideCredits()
    {
        _creditsAnim.SetTrigger("HideCredits");
        _mainMenuAnim.SetTrigger("ShowMainMenu");
    }

    public void ShowHowToPlay()
    {
        _mainMenuAnim.ResetTrigger("ShowMainMenu");
        _mainMenuAnim.SetTrigger("HideMainMenu");
        _howToPlayAnim.SetTrigger("ShowHowToPlay");
    }

    public void HideHowToPlay()
    {
        _howToPlayAnim.SetTrigger("HideHowToPlay");
        _mainMenuAnim.SetTrigger("ShowMainMenu");
    }
}
