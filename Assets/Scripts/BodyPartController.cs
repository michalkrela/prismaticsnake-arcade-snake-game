using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class BodyPartController : MonoBehaviour
{
    public Vector2Int GridPosition;
    [SerializeField]
    private GlowColorBlueprint _glow;
    public GlowColorBlueprint Glow
    {
        get { return _glow; }
        set
        {
            _glow = value;
            GetComponentInChildren<Light2D>().color = Glow.GlowColor;
        }
    }

    public void SetGridPosition(Vector2Int gridPos)
    {
        GridPosition = gridPos;
        transform.position = new Vector3(gridPos.x, gridPos.y);
    }
}
