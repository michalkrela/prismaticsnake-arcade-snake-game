using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GlowColorData", menuName = "Glow Color Data", order = 51)]
public class GlowColorBlueprint : ScriptableObject
{
    public Color GlowColor;
    public GlowColor GlowDescription;

}

public enum GlowColor
{
    Red = 0,
    Orange = 1,
    Yellow = 2,
    Green = 3,
    Blue = 4,
    Indigo = 5,
    Violet = 6,
}
