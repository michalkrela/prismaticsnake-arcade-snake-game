using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class FoodController : MonoBehaviour
{
    [SerializeField]
    private GlowColorBlueprint _glow;
    public GlowColorBlueprint Glow
    {
        get { return _glow; }
        set
        {
            _glow = value;
            GetComponentInChildren<Light2D>().color = Glow.GlowColor;
            GetComponentInChildren<SpriteRenderer>().color = Glow.GlowColor;
        }
    }

    private GameManager GM;
    private Animator anim;

    // Start is called before the first frame update
    private void Awake()
    {
        var gm = GameObject.FindGameObjectWithTag("GameManager");
        if (gm != null)
        {
            GM = gm.GetComponent<GameManager>();
        }

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GM.ScorePoint(Glow);
            GetComponentInChildren<SpriteRenderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;
            anim.SetTrigger("Absorbed");
            Destroy(this.gameObject, 5f);
        }
    }
}
