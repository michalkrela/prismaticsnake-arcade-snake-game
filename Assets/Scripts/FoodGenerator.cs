using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FoodGenerator : MonoBehaviour
{
    public GameObject FoodPrefab;
    public List<GlowColorBlueprint> GlowColors;

    public PlayerController Player;

    private int _playGroundWidth;
    private int _playGroundHeight;

    // Start is called before the first frame update
    void Awake()
    {
        _playGroundHeight = (int)GetComponent<GameManager>().PlayGroundHeight;
        _playGroundWidth = (int)GetComponent<GameManager>().PlayGroundWidth;

        GenerateFood();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GenerateFood(GlowColor? glow = null)
    {
        Vector2Int gridPos;

        do
        {
            gridPos = new Vector2Int(Random.Range(0, _playGroundWidth),
                                     Random.Range(0, _playGroundHeight));

        } while (Player.WholeBodyPositions.IndexOf(gridPos) != -1);

        var food = Instantiate(FoodPrefab, new Vector3(gridPos.x, gridPos.y, 0), Quaternion.identity);

        if (glow == null)
        {
            food.GetComponent<FoodController>().Glow = GetRandomGlow();
        }
        else
        {
            food.GetComponent<FoodController>().Glow = GlowColors.FirstOrDefault(x => x.GlowDescription == glow);
        }

        return food;
    }

    public void GenerateFoodForCombo(GlowColor nextComboColor)
    {
        Destroy(GenerateFood(), 5f);
        GenerateFood(nextComboColor);
    }

    public GlowColorBlueprint GetRandomGlow()
    {
        return GlowColors[Random.Range(0, GlowColors.Count)];
    }
}
