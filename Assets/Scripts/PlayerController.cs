using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("References")]
    public GameManager GameManager;

    [Header("Movement Related")]
    public Vector2Int HeadGridPosition;
    public MovementDirection MoveDirection = MovementDirection.Right;
    public float MovementSpeed = 1f;

    [Header("Snake Body Related")]
    public GameObject BodyPartPrefab;
    public int BodySize = 5;
    public List<Vector2Int> SnakePositionHistory = new List<Vector2Int>();
    public List<GameObject> SnakeBodyElements = new List<GameObject>();

    [HideInInspector]
    public List<Vector2Int> WholeBodyPositions
    {
        get
        {
            var bodyPos = new List<Vector2Int>() { HeadGridPosition };
            bodyPos.AddRange(SnakePositionHistory);

            return bodyPos;
        }
    }

    private bool _moved = false;
    private bool _alive = true;
    private bool _gamePaused = false;

    // Start is called before the first frame update
    private void Awake()
    {
        HeadGridPosition = new Vector2Int(14, 14);
        for (int i = 0; i < BodySize; i++)
        {
            CreateBodyPart(HeadGridPosition, GameManager.GetComponent<FoodGenerator>().GetRandomGlow());
            SnakePositionHistory.Insert(0, HeadGridPosition);
        }

        StartCoroutine(MoveGenerator());
    }

    // Update is called once per frame
    void Update()
    {
        PlayerInput();
        PlayerMovement();
        PlayerCheats();
    }

    private void PlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.PauseUnpauseGame();
            _gamePaused = !_gamePaused;
        }
    }

    private void PlayerCheats()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            BodySize++;

            CreateBodyPart(SnakePositionHistory[0], GameManager.GetComponent<FoodGenerator>().GetRandomGlow());
        }
    }

    #region Movement Related
    public void PlayerMovement()
    {
        if (!_moved && !_gamePaused)
        {
            if (Input.GetKeyDown(KeyCode.A) && MoveDirection != MovementDirection.Right)
            {
                MoveDirection = MovementDirection.Left;
                _moved = true;
            }
            if (Input.GetKeyDown(KeyCode.W) && MoveDirection != MovementDirection.Down)
            {
                MoveDirection = MovementDirection.Up;
                _moved = true;
            }
            if (Input.GetKeyDown(KeyCode.D) && MoveDirection != MovementDirection.Left)
            {
                MoveDirection = MovementDirection.Right;
                _moved = true;
            }
            if (Input.GetKeyDown(KeyCode.S) && MoveDirection != MovementDirection.Up)
            {
                MoveDirection = MovementDirection.Down;
                _moved = true;
            }
        }
    }

    public IEnumerator MoveGenerator()
    {
        while (_alive)
        {
            SnakePositionHistory.Insert(0, HeadGridPosition);

            switch (MoveDirection)
            {
                case MovementDirection.Left:
                    HeadGridPosition.x--;
                    break;
                case MovementDirection.Up:
                    HeadGridPosition.y++;
                    break;
                case MovementDirection.Right:
                    HeadGridPosition.x++;
                    break;
                case MovementDirection.Down:
                    HeadGridPosition.y--;
                    break;
                default:
                    break;
            }

            HeadGridPosition = CheckPlayGroundWrap(HeadGridPosition);

            if (SnakePositionHistory.Count >= BodySize + 1)
            {
                SnakePositionHistory.RemoveAt(SnakePositionHistory.Count - 1);
            }

            transform.position = new Vector3(HeadGridPosition.x, HeadGridPosition.y, 0);
            transform.eulerAngles = new Vector3(0, 0, GetRotation(MoveDirection) - 90);

            for (int i = 0; i < SnakePositionHistory.Count; i++)
            {
                if (SnakeBodyElements.Count >= SnakePositionHistory.Count)
                {
                    SnakeBodyElements[i].GetComponent<BodyPartController>().SetGridPosition(SnakePositionHistory[i]);
                }
            }

            _moved = false;

            yield return new WaitForSeconds(MovementSpeed);
        }
    }

    private Vector2Int CheckPlayGroundWrap(Vector2Int gridPosition)
    {
        if (gridPosition.x < 0)
        {
            gridPosition.x = GameManager.PlayGroundWidth - 1;
        }
        if (gridPosition.x > GameManager.PlayGroundWidth - 1)
        {
            gridPosition.x = 0;
        }
        if (gridPosition.y < 0)
        {
            gridPosition.y = GameManager.PlayGroundHeight - 1;
        }
        if (gridPosition.y > GameManager.PlayGroundHeight - 1)
        {
            gridPosition.y = 0;
        }

        return gridPosition;
    }

    private float GetRotation(MovementDirection moveDirection)
    {
        switch (MoveDirection)
        {
            case MovementDirection.Left:
                return -90f;
            case MovementDirection.Up:
                return 180f;
            case MovementDirection.Right:
                return 90f;
            case MovementDirection.Down:
                return 360f;
            default:
                return 0f;
        }
    }
    #endregion

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Food"))
        {
            BodySize++;
            MovementSpeed -= 0.001f;

            CreateBodyPart(SnakePositionHistory[0], collision.GetComponent<FoodController>().Glow);
        }

        if (collision.CompareTag("BodyPart"))
        {
            _alive = false;
            GameManager.SetGameOver();
        }
    }

    private void CreateBodyPart(Vector2Int startPos, GlowColorBlueprint glow)
    {
        var bodyPart = Instantiate(BodyPartPrefab, new Vector3(startPos.x, startPos.y, 0), Quaternion.identity, transform);
        bodyPart.GetComponent<BodyPartController>().Glow = glow;
        bodyPart.GetComponent<BodyPartController>().GridPosition = startPos;
        SnakeBodyElements.Add(bodyPart);
    }
}

public enum MovementDirection
{
    Left,
    Up,
    Right,
    Down
}