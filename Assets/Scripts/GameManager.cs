using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("UI References")]
    public Image ScorePanel;
    public TMP_Text ScoreText;

    public Image ComboPanel;
    public TMP_Text ComboText;

    public Canvas GameOverCanvas;
    public Canvas PauseCanvas;
    public TMP_Text HighestComboText;

    [Header("General")]
    [SerializeField]
    private int _score;
    public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            ScoreText.text = _score.ToString();
        }
    }

    [SerializeField]
    private int _comboValue;
    public int ComboValue
    {
        get { return _comboValue; }
        set
        {
            _comboValue = value;
            ComboText.text = string.Format("X{0}", _comboValue.ToString());
        }
    }

    [SerializeField]
    private GlowColor _nextComboGlow;
    public GlowColor NextComboGlow
    {
        get { return _nextComboGlow; }
        set
        {
            if ((int)value > (int)GlowColor.Violet)
            {
                _nextComboGlow = GlowColor.Red;
            }
            else
            {
                _nextComboGlow = value;
            }

            ComboPanel.color = _foodGenerator.GlowColors
                                             .FirstOrDefault(x => x.GlowDescription == _nextComboGlow)
                                             .GlowColor;
        }
    }

    public GameObject PlayGround;

    [HideInInspector]
    public int PlayGroundWidth;

    [HideInInspector]
    public int PlayGroundHeight;

    private FoodGenerator _foodGenerator;
    private int _highestCombo = 0;

    // Start is called before the first frame update
    void Awake()
    {
        _foodGenerator = GetComponent<FoodGenerator>();

        Score = 0;
        ComboValue = 0;

        NextComboGlow = _foodGenerator.GetRandomGlow().GlowDescription;

        PlayGroundHeight = (int)PlayGround.transform.localScale.y - 2;
        PlayGroundWidth = (int)PlayGround.transform.localScale.x - 2;
    }

    public void ScorePoint(GlowColorBlueprint glow)
    {
        CheckComboProgress(glow);

        var comboMultiplier = ComboValue > 0 ? ComboValue : 1;
        Score += 10 * comboMultiplier;
        ScorePanel.color = glow.GlowColor;

        _foodGenerator.GenerateFoodForCombo(NextComboGlow);
        Debug.LogFormat("Score: {0}", Score);
    }

    private void CheckComboProgress(GlowColorBlueprint glow)
    {
        if (NextComboGlow == glow.GlowDescription)
        {
            NextComboGlow++;
            ComboValue++;
            if (ComboValue > _highestCombo)
            {
                _highestCombo = ComboValue;
            }
        }
        else
        {
            ComboValue = 0;
        }
    }

    public void SetGameOver()
    {
        GameOverCanvas.enabled = true;
        HighestComboText.text = string.Format("HIGHEST COMBO: {0}", _highestCombo);
    }

    public void RestartGame()
    {
        GameOverCanvas.enabled = false;
        SceneManager.LoadScene(1);
    }

    public void PauseUnpauseGame()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            PauseCanvas.enabled = true;
        }
        else
        {
            Time.timeScale = 1;
            PauseCanvas.enabled = false;
        }
    }
}